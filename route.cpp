
#include <iostream>
#include <vector>
#include <map>
#include <string>
using namespace std;


struct Node
{
public:
    string name;
    int id;
    int cost;
    string node_type;
    //vector<int> queue;
    Node(string o_name, int o_id, int o_cost, string o_node_type)
    {
        name = o_name;
        id = o_id;
        cost = o_cost;
        node_type = o_node_type;
    }
};

struct Edge
{
    int src, dist;
};

struct Actor
{
public:
    string name;
    int cur_loc;
    int start_loc;
    int end_loc;
    Actor(string o_name, int o_cur_loc, int o_start_loc, int o_end_loc)
    {
        name = o_name;
        cur_loc = o_cur_loc;
        start_loc = o_start_loc;
        end_loc = o_end_loc;
    }
};

struct Graph
{
public:
    //vector<Node> nodes;
    map<int, Node*> nodes;
    map<int, vector<int>> edges;
    vector<Actor> actors;
    Graph(vector<Node> o_nodes, vector<Edge> o_edges, vector<Actor> o_actor)
    {
        for (auto &node: o_nodes)
        {
            nodes[node.id] = &node;
        }
        for (auto &edge: o_edges)
        {
            edges[edge.src].push_back(edge.dist);
        }
        for (auto &actor: o_actor)
        {
            actors.push_back(actor);
        }
    }

    void displayEdges()
    {
        for (auto pair: edges)
        {
            cout<<pair.first<<" -> ";
            for (auto d: pair.second)
            {
                cout<<d<<", ";
            }
            cout<<endl;
        }
    }

    void displayNodes()
    {
        for (auto pair: nodes)
        {
            Node node = *pair.second;
            cout<<"--------------------"<<endl;
            cout<<node.name<<", id: "<<node.id<<", cost: "<<node.cost<<", node type: "<<node.node_type<<endl;
        }
    }

    void displayActors()
    {
        for (auto actor: actors)
        {
            cout<<actor.name<<" cur in node "<<actor.cur_loc<<", ("<<actor.start_loc<<" -> "<<actor.end_loc<<")"<<endl;
        }
    }

    // map<vector<int>> decisionMaker()
    // {
    //     //假设每个人知道当前通道的拥堵情况，但是不能预测自己到达目标通道时通道的拥堵情况。
    //     //A*规划最短路径的同时进行调度
    //     //对每个通道来说，每有一辆车出通道（入通道）进队列，
    //     //都要修改每个tunnel的通过时间（当前通道等待队列的车辆数*通过时间+通道内车辆的剩余通过时间），
    //     //这个时间作为整个通道的通过时间，排队的车越多，通过时间越长
    //     //每修改过通道的通过时间，需要对队列中每辆车重新规划最短（最快）路径
    //     //对每个通道的队列来说，优先取最前的车规划路线，然后规划排第二的车，直到全部规划完
    //     //如果新路径无法比当前路径更快，保持当前路径
    //     for (auto actor: actors)
    //     {
    //         vector<int> blocked_node;
    //         vector<int> possible_branch;
    //         findFastPath(actor, blocked_node, possible_branch);
    //     }

    // }

    // vector<int> findFastPath(actor, vector<int> blocked_node, vector<int> possible_branch)
    // {
    //     cur_loc = actor.cur_loc;
    //     distination = actor.end_loc;
    //     blocked_node = blocked_node;
    //     possible_branch = possible_branch;



    // }

};

// struct Strategy
// {
// public:
//     int 
// }



int main()
{

    //data
    vector<Node> nodes = 
    {
        {"Humpty Office -> Tunnel Entrance", 0, 10, "unlimit"},
        {"Dumpty Office -> Tunnel Entrance", 1, 5, "unlimit"},
        {"Tunnel Entrance -> Tunnel Exit", 2, 5, "limit"},
        {"Tunnel Exit -> Humpty Home", 3, 15, "unlimit"},
        {"Tunnel Exit -> Dumpty Home", 4, 20, "unlimit"}
    };
    vector<Edge> edges = 
    {
        {0, 2}, {1, 2}, {2, 3}, {2, 4}
    };
    vector<Actor> actors = 
    {
        {"Humpty", 0, 0, 3},
        {"Dumpty", 1, 1, 4}
    };
    Graph graph(nodes, edges, actors);


    graph.displayEdges();
    graph.displayNodes();
    graph.displayActors();
    return 0;
}













